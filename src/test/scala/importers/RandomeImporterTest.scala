package importers

import App.importers.ImageImporter.RandomImageImporter
import App.models.Image.RGBImage
import org.scalatest.FunSuite

class RandomeImporterTest extends FunSuite{

  def checkIfValid(img: RGBImage , w : Int , h : Int): Boolean = {

    if (img.grid.pixels.size != h)
      return false
    for (i <- img.grid.pixels)
      if(i.size != w)
        return false

    for (row <- img.grid.pixels)
      for(i <- row) {
        for (j <- i.get()) {
          if (j < 0 || j > 255)
            return false
        }
      }
    true
  }

  test("Generates not empty image") {
    val importer = RandomImageImporter(10,10)
    val img = importer.importItem()
    assert(img.nonEmpty)
    assert(checkIfValid(img.get, 10 , 10))
  }
  test("two images are not same") {
    val importer = new RandomImageImporter(10, 10)
    val img = importer.importItem()
    val img2 = importer.importItem()
    assert(img.get.grid != img2.get.grid)
    assert(checkIfValid(img.get, 10 , 10))
    assert(checkIfValid(img2.get, 10 , 10))
  }

  test("image with different size") {
    val importer = RandomImageImporter(200, 10)
    val img = importer.importItem()
    assert(checkIfValid(img.get, 200 , 10))
  }

  test("image with different size, neg value") {
    val importer = RandomImageImporter(-200, 10)
    val img = importer.importItem()
    assert(checkIfValid(img.get, 200 , 10))

  }

  test("negative value in constructor") {
    val importer = RandomImageImporter(-10, -10)
    val img = importer.importItem()
    val img2 = importer.importItem()
    assert(img.get.grid != img2.get.grid)
    assert(checkIfValid(img.get, 10 , 10))
    assert(checkIfValid(img2.get, 10 , 10))

  }

}
