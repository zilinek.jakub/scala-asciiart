package importers

import App.importers.ImageImporter.RGBImageFileImporter
import App.models.Pixel.RGB_Pixel
import org.scalatest.{BeforeAndAfterEach, FunSuite}
import java.nio.file.FileSystems


class RGBImageImporterTest extends FunSuite with BeforeAndAfterEach {

  override protected def beforeEach(): Unit  = {

  }


  test ("invalid file to import"){
    val image = RGBImageFileImporter("src/test/images/invalid.txt").importItem()
    assert(image.isEmpty)
  }

  test ("invalid path to import"){
    val image = RGBImageFileImporter("HelloImSuperPepeHandsUp.png").importItem()
    assert(image.isEmpty)
  }

  test("import from success"){
    val image = RGBImageFileImporter("src/test/scala/images/pepe.png").importItem()
    assert(image.nonEmpty)
  }

  test("import from success, png with .. in path"){
    val image = RGBImageFileImporter("src/test/scala/images/../../scala/images/pepe.png").importItem()
    assert(image.nonEmpty)
  }

  test("import from success, check values white"){
    val image = RGBImageFileImporter("src/test/scala/images/2x2.png").importItem()
    assert(image.nonEmpty)
    val ref = List(List(RGB_Pixel(List(255,255,255)), RGB_Pixel(List(255,255,255))), List(RGB_Pixel(List(255,255,255)),RGB_Pixel(List(255,255,255))))
    assert(image.get.grid.pixels == ref)
  }

  test("import from success, check values blue"){
    val image = RGBImageFileImporter("src/test/scala/images/2x2_blue.jpg").importItem()
    assert(image.nonEmpty)
    val ref = List(List(RGB_Pixel(List(129,214,255)), RGB_Pixel(List(129,214,255))), List(RGB_Pixel(List(129,214,255)),RGB_Pixel(List(129,214,255))))
    assert(image.get.grid.pixels == ref)
  }

  test("import from success, check values blue with .. in path"){
    val image = RGBImageFileImporter("src/test/../../src/test/scala/images/2x2_blue.jpg").importItem()
    assert(image.nonEmpty)
    val ref = List(List(RGB_Pixel(List(129,214,255)), RGB_Pixel(List(129,214,255))), List(RGB_Pixel(List(129,214,255)),RGB_Pixel(List(129,214,255))))
    assert(image.get.grid.pixels == ref)
  }

  test("absolutePath"){
    val absolutePath = FileSystems.getDefault.getPath("src/test/../../src/test/scala/images/2x2_blue.jpg").normalize.toAbsolutePath.toString
    println(absolutePath)
    val image = RGBImageFileImporter(absolutePath).importItem()
    assert(image.nonEmpty)
    val ref = List(List(RGB_Pixel(List(129,214,255)), RGB_Pixel(List(129,214,255))), List(RGB_Pixel(List(129,214,255)),RGB_Pixel(List(129,214,255))))
    assert(image.get.grid.pixels == ref)
  }
}
