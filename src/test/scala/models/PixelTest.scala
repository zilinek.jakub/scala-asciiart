package models

import App.models.Pixel.{Char_Pixel, Gray_Pixel, RGB_Pixel}
import org.scalatest.FunSuite

class PixelTest extends FunSuite{

  test("CharPixel"){
    val pix = Char_Pixel('1')
    assert(pix.getChar() sameElements Array('1'))
    assert(pix.get() == '1')
  }

  test("RGBPixel"){
    val pix = RGB_Pixel(List(1,2,3))
    assert(pix.getChar() sameElements Array('1','2','3'))
    assert(pix.get() == List(1,2,3))
  }

  test("GrayPixel"){
    val pix = Gray_Pixel(1)
    assert(pix.getChar() sameElements Array('1'))
    assert(pix.get() == 1)
  }

}
