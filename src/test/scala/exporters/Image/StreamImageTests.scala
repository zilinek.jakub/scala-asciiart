package exporters.Image

import App.exporters.Image.StreamImageExporter.StreamImageExporter
import App.exporters.Text.StreamTextExporter
import App.models.Grid.Grid
import App.models.Image.{CharImage, GrayImage, RGBImage}
import App.models.Pixel.{Char_Pixel, Gray_Pixel, RGB_Pixel}
import org.scalatest.FunSuite

import java.io.ByteArrayOutputStream

class StreamImageTests extends FunSuite {
  test("[CharImage]Write"){
    val stream = new ByteArrayOutputStream()
    val exporter = StreamImageExporter(stream)
    val img = CharImage(Grid[Char_Pixel](List(List(Char_Pixel('1'),Char_Pixel('2'),Char_Pixel('3')))))

    exporter.export(img)

    assert(stream.toString("UTF-8") == "123\n")
  }
  test("[CharImage]empty image"){
    val stream = new ByteArrayOutputStream()
    val exporter = StreamImageExporter(stream)

    exporter.export(CharImage(Grid[Char_Pixel](List(List()))))

    assert(stream.toString("UTF-8") == "\n")
  }
  test("[CharImage]write multiple rows image"){
    val stream = new ByteArrayOutputStream()
    val exporter = StreamImageExporter(stream)
    val img = CharImage(Grid[Char_Pixel](List(
      List(Char_Pixel('1'),Char_Pixel('2'),Char_Pixel('3')),
      List(Char_Pixel('1'),Char_Pixel('2'),Char_Pixel('3'))
    )))
    exporter.export(img)

    assert(stream.toString("UTF-8") == "123\n123\n")
  }

  test("[RGBImage]Write"){
    val stream = new ByteArrayOutputStream()
    val exporter = StreamImageExporter(stream)
    val img = RGBImage(Grid[RGB_Pixel](List(List(RGB_Pixel(List(1,2,3)),RGB_Pixel(List(1,2,3)),RGB_Pixel(List(1,2,3))))))

    exporter.export(img)

    assert(stream.toString("UTF-8") == "123123123\n")
  }
  test("[RGBImage]empty image"){
    val stream = new ByteArrayOutputStream()
    val exporter = StreamImageExporter(stream)

    exporter.export(RGBImage(Grid[RGB_Pixel](List(List()))))

    assert(stream.toString("UTF-8") == "\n")
  }
  test("[RGBImage]write multiple rows image"){
    val stream = new ByteArrayOutputStream()
    val exporter = StreamImageExporter(stream)
    val img = RGBImage(Grid[RGB_Pixel](List(
      List(RGB_Pixel(List(1,2,3)),RGB_Pixel(List(1,2,3)),RGB_Pixel(List(1,2,3))),
      List(RGB_Pixel(List(1,2,3)),RGB_Pixel(List(1,2,3)),RGB_Pixel(List(1,2,3)))
    )))
    exporter.export(img)

    assert(stream.toString("UTF-8") == "123123123\n123123123\n")
  }

  test("[RGBImage]write multiple rows image with 4 values in List"){
    val stream = new ByteArrayOutputStream()
    val exporter = StreamImageExporter(stream)
    val img = RGBImage(Grid[RGB_Pixel](List(
      List(RGB_Pixel(List(1,2,3,4)),RGB_Pixel(List(1,2,3)),RGB_Pixel(List(1,2,3))),
      List(RGB_Pixel(List(1,2,3)),RGB_Pixel(List(1,2,3)),RGB_Pixel(List(1,2,3)))
    )))
    exporter.export(img)

    assert(stream.toString("UTF-8") == "123123123\n123123123\n")
  }

  test("[GrayImage]Write"){
    val stream = new ByteArrayOutputStream()
    val exporter = StreamImageExporter(stream)
    val img = GrayImage(Grid[Gray_Pixel](List(List(Gray_Pixel(1),Gray_Pixel(2),Gray_Pixel(3)))))

    exporter.export(img)

    assert(stream.toString("UTF-8") == "123\n")
  }
  test("[GrayImage]empty image"){
    val stream = new ByteArrayOutputStream()
    val exporter = StreamImageExporter(stream)

    exporter.export(GrayImage(Grid[Gray_Pixel](List(List()))))

    assert(stream.toString("UTF-8") == "\n")
  }
  test("[GrayImage]write multiple rows image"){
    val stream = new ByteArrayOutputStream()
    val exporter = StreamImageExporter(stream)
    val img = GrayImage(Grid[Gray_Pixel](List(
      List(Gray_Pixel(255),Gray_Pixel(0),Gray_Pixel(3)),
      List(Gray_Pixel(1),Gray_Pixel(2),Gray_Pixel(3))
    )))
    exporter.export(img)

    assert(stream.toString("UTF-8") == "25503\n123\n")
  }
}
