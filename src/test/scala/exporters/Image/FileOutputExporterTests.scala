package exporters.Image

import App.exporters.Image.StreamImageExporter.FileOutExporter
import App.models.Grid.Grid
import App.models.Image.{CharImage, GrayImage, RGBImage}
import App.models.Pixel.{Char_Pixel, Gray_Pixel, RGB_Pixel}
import exporters.helpers.TestWithFiles
import org.scalatest.FunSuite

import java.io.File

class FileOutputExporterTests extends FunSuite
  with TestWithFiles{

  test("[CharImage]No file exists"){
    val fileName = getTestFile
    val img = CharImage(Grid[Char_Pixel](List(List(Char_Pixel('1'),Char_Pixel('2'),Char_Pixel('3')))))

    try {
      ensureDeleted(fileName)

      val file = new File(fileName)
      val exporter = new FileOutExporter(file)

      exporter.export(img)
      exporter.close()

      assertFileContent(fileName, "123\n")
    }
    finally {
      ensureDeleted(fileName)
    }
  }

  test("[CharImage]File already exists"){
    val fileName = getTestFile
    val img = CharImage(Grid[Char_Pixel](List(List(Char_Pixel('1'),Char_Pixel('2'),Char_Pixel('3')))))

    try{
      ensureCreated(fileName)

      val file = new File(fileName)
      val exporter = new FileOutExporter(file)

      exporter.export(img)
      exporter.close()

      assertFileContent(fileName, "123\n")
    }
    finally{
      ensureDeleted(fileName)
    }
  }

  test("[CharImage]No file exists more text"){
    val fileName = getTestFile
    val img = CharImage(Grid[Char_Pixel](List(
      List(Char_Pixel('1'),Char_Pixel('2'),Char_Pixel('3')),
      List(Char_Pixel('1'),Char_Pixel('2'),Char_Pixel('3'))
    )))

    try {
      ensureDeleted(fileName)

      val file = new File(fileName)
      val exporter = new FileOutExporter(file)

      exporter.export(img)
      exporter.close()

      assertFileContent(fileName, "123\n123\n")
    }
    finally {
      ensureDeleted(fileName)
    }
  }

  test("[CharImage]File already exists more text"){
    val fileName = getTestFile
    val img = CharImage(Grid[Char_Pixel](List(
      List(Char_Pixel('1'),Char_Pixel('2'),Char_Pixel('3')),
      List(Char_Pixel('1'),Char_Pixel('2'),Char_Pixel('3'))
    )))

    try{
      ensureCreated(fileName)

      val file = new File(fileName)
      val exporter = new FileOutExporter(file)

      exporter.export(img)
      exporter.close()

      assertFileContent(fileName, "123\n123\n")
    }
    finally{
      ensureDeleted(fileName)
    }
  }

  test("[RGBImage]No file exists"){
    val fileName = getTestFile
    val img = RGBImage(Grid[RGB_Pixel](List(List(RGB_Pixel(List(1,2,3)),RGB_Pixel(List(4,5,6)),RGB_Pixel(List(7,8,9))))))

    try {
      ensureDeleted(fileName)

      val file = new File(fileName)
      val exporter = new FileOutExporter(file)

      exporter.export(img)
      exporter.close()

      assertFileContent(fileName, "123456789\n")
    }
    finally {
      ensureDeleted(fileName)
    }
  }

  test("[RGBImage]File already exists"){
    val fileName = getTestFile
    val img = RGBImage(Grid[RGB_Pixel](List(List(RGB_Pixel(List(1,2,3)),RGB_Pixel(List(4,5,6)),RGB_Pixel(List(7,8,9))))))


    try{
      ensureCreated(fileName)

      val file = new File(fileName)
      val exporter = new FileOutExporter(file)

      exporter.export(img)
      exporter.close()

      assertFileContent(fileName, "123456789\n")
    }
    finally{
      ensureDeleted(fileName)
    }
  }

  test("[RGBImage]No file exists more text"){
    val fileName = getTestFile
    val img = RGBImage(Grid[RGB_Pixel](List(
      List(RGB_Pixel(List(1,2,3)),RGB_Pixel(List(4,5,6)),RGB_Pixel(List(7,8,9))),
      List(RGB_Pixel(List(255,0,255)),RGB_Pixel(List(255,0,255)),RGB_Pixel(List(255,0,255))))))


    try {
      ensureDeleted(fileName)

      val file = new File(fileName)
      val exporter = new FileOutExporter(file)

      exporter.export(img)
      exporter.close()

      assertFileContent(fileName, "123456789\n255025525502552550255\n")
    }
    finally {
      ensureDeleted(fileName)
    }
  }

  test("[RGBImage]File already exists more text"){
    val fileName = getTestFile
    val img = RGBImage(Grid[RGB_Pixel](List(
      List(RGB_Pixel(List(1,2,3)),RGB_Pixel(List(4,5,6)),RGB_Pixel(List(7,8,9))),
      List(RGB_Pixel(List(255,0,255)),RGB_Pixel(List(255,0,255)),RGB_Pixel(List(255,0,255))))))

    try{
      ensureCreated(fileName)

      val file = new File(fileName)
      val exporter = new FileOutExporter(file)

      exporter.export(img)
      exporter.close()

      assertFileContent(fileName, "123456789\n255025525502552550255\n")

    }
    finally{
      ensureDeleted(fileName)
    }
  }

  test("[GrayImage]No file exists"){
    val fileName = getTestFile
    val img = GrayImage(Grid[Gray_Pixel](List(List(Gray_Pixel(1),Gray_Pixel(2),Gray_Pixel(3)))))

    try {
      ensureDeleted(fileName)

      val file = new File(fileName)
      val exporter = new FileOutExporter(file)

      exporter.export(img)
      exporter.close()

      assertFileContent(fileName, "123\n")
    }
    finally {
      ensureDeleted(fileName)
    }
  }

  test("[GrayImage]File already exists"){
    val fileName = getTestFile
    val img = GrayImage(Grid[Gray_Pixel](List(List(Gray_Pixel(1),Gray_Pixel(2),Gray_Pixel(3)))))


    try{
      ensureCreated(fileName)

      val file = new File(fileName)
      val exporter = new FileOutExporter(file)

      exporter.export(img)
      exporter.close()

      assertFileContent(fileName, "123\n")
    }
    finally{
      ensureDeleted(fileName)
    }
  }

  test("[GrayImage]No file exists more text"){
    val fileName = getTestFile
    val img = GrayImage(Grid[Gray_Pixel](List(
      List(Gray_Pixel(1),Gray_Pixel(2),Gray_Pixel(3)),
      List(Gray_Pixel(1),Gray_Pixel(0),Gray_Pixel(255))
    )))

    try {
      ensureDeleted(fileName)

      val file = new File(fileName)
      val exporter = new FileOutExporter(file)

      exporter.export(img)
      exporter.close()

      assertFileContent(fileName, "123\n10255\n")
    }
    finally {
      ensureDeleted(fileName)
    }
  }

  test("[GrayImage]File already exists more text"){
    val fileName = getTestFile
    val img = GrayImage(Grid[Gray_Pixel](List(
      List(Gray_Pixel(1),Gray_Pixel(2),Gray_Pixel(3)),
      List(Gray_Pixel(1),Gray_Pixel(0),Gray_Pixel(255))
    )))

    try{
      ensureCreated(fileName)

      val file = new File(fileName)
      val exporter = new FileOutExporter(file)

      exporter.export(img)
      exporter.close()

      assertFileContent(fileName, "123\n10255\n")
    }
    finally{
      ensureDeleted(fileName)
    }
  }
}
