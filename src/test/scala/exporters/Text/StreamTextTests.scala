package exporters.Text

import App.exporters.Text.StreamTextExporter
import org.scalatest.FunSuite

import java.io.ByteArrayOutputStream

class StreamTextTests extends FunSuite {
  test("Write"){
    val stream = new ByteArrayOutputStream()
    val exporter = new StreamTextExporter(stream)

    exporter.export("YooooPepega")

    assert(stream.toString("UTF-8") == "YooooPepega")
  }
  test("empty string"){
    val stream = new ByteArrayOutputStream()
    val exporter = new StreamTextExporter(stream)

    exporter.export("")

    assert(stream.toString("UTF-8") == "")
  }
  test("white space chars test"){
    val stream = new ByteArrayOutputStream()
    val exporter = new StreamTextExporter(stream)

    exporter.export("test\ntest\n\ntest\t\n")

    assert(stream.toString("UTF-8") == "test\ntest\n\ntest\t\n")
  }
}
