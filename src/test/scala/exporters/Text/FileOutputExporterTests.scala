package exporters.Text

import App.exporters.Text.FileOutputExporter
import exporters.helpers.TestWithFiles
import org.scalatest.FunSuite

import java.io.File

class FileOutputExporterTests extends FunSuite
  with TestWithFiles{

  test("No file exists"){
    val fileName = getTestFile

    try {
      ensureDeleted(fileName)

      val file = new File(fileName)
      val exporter = new FileOutputExporter(file)

      exporter.export("YooooPepega")
      exporter.close()

      assertFileContent(fileName, "YooooPepega")
    }
    finally {
      ensureDeleted(fileName)
    }
  }

  test("File already exists"){
    val fileName = getTestFile

    try{
      ensureCreated(fileName)

      val file = new File(fileName)
      val exporter = new FileOutputExporter(file)

      exporter.export("YooooPepega")
      exporter.close()

      assertFileContent(fileName, "YooooPepega")
    }
    finally{
      ensureDeleted(fileName)
    }
  }

  test("No file exists more text"){
    val fileName = getTestFile

    try {
      ensureDeleted(fileName)

      val file = new File(fileName)
      val exporter = new FileOutputExporter(file)

      exporter.export("test\ntest\n\ntest\t\n")
      exporter.close()

      assertFileContent(fileName, "test\ntest\n\ntest\t\n")
    }
    finally {
      ensureDeleted(fileName)
    }
  }

  test("File already exists more text"){
    val fileName = getTestFile

    try{
      ensureCreated(fileName)

      val file = new File(fileName)
      val exporter = new FileOutputExporter(file)

      exporter.export("test\ntest\n\ntest\t\n")
      exporter.close()

      assertFileContent(fileName, "test\ntest\n\ntest\t\n")
    }
    finally{
      ensureDeleted(fileName)
    }
  }
}
