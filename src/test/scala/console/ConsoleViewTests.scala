package console

import App.console.controllers.{ConsoleController, Controller}
import App.console.views.ConsoleView
import App.exporters.Image.StreamImageExporter.StreamImageExporter
import App.filters.charImage.mixed.MixedFilter
import App.models.Grid.Grid
import App.models.Image.RGBImage
import App.models.Pixel.RGB_Pixel
import org.mockito.MockitoSugar.{mock, verify, when}
import org.mockito.captor.ArgCaptor
import org.scalatest.{BeforeAndAfterEach, FunSuite}

import scala.collection.mutable.ListBuffer

class ConsoleViewTests extends FunSuite with BeforeAndAfterEach{

  var mockController : Controller = _
  var view : ConsoleView = _
  var img: Option[RGBImage] = _
  var output : ListBuffer[StreamImageExporter] = ListBuffer()

  /**predefine that test.png exists, all calls with this img will be processed as imported*/
  override def beforeEach(): Unit = {
    mockController = mock[ConsoleController]
    view = new ConsoleView(mockController)
    img = Option(RGBImage(Grid[RGB_Pixel](List(List(RGB_Pixel(List(255,255,255)))))))
    when(mockController.importImage("test.png")).thenReturn(img)
    when(mockController.importImage("test-invalid.png")).thenReturn(Option.empty)
  }

  test("empty args"){
    view.processCommand(List())
    verify(mockController).showHelp()
    verify(mockController).showError("Invalid image type or path to image\n\n")
  }

  test("invalid args"){
    view.processCommand(List("-image", "test.png", "--output-console"))
    verify(mockController).showHelp()
    verify(mockController).showError("Invalid image type or path to image\n\n")
  }

  test("help"){
    view.processCommand(List("-image", "test.png", "--output-console"))
    view.processCommand(List("help"))
  }

  test("--image no path"){
    view.processCommand(List("--image"))
    verify(mockController).showHelp()
    verify(mockController).showError("Invalid image type or path to image\n\n")
  }

  test("--image-random test.png --output-console"){
    view.processCommand(List("--image-random", "test.png", "--output-console"))
    verify(mockController).showError("Some of arguments are not used, or not use but not properly:")
    verify(mockController).showError("\ttest.png")
    verify(mockController).showHelp()
  }

  test("--image test-invalid.png invalid path"){
    view.processCommand(List("--image", "test-invalid.png"))
    verify(mockController).showHelp()
    verify(mockController).showError("Invalid image type or path to image\n\n")
  }

  test("--image test.png no endpoint"){
    view.processCommand(List("--image", "test.png"))
    verify(mockController).showHelp()
    verify(mockController).showError("Output endpoint is not specified...")
  }

  test("--image test.png --output-console"){
    view.processCommand(List("--image", "test.png", "--output-console"))
    verify(mockController).importImage("test.png")
    val filter = ArgCaptor[MixedFilter]
    val out = ArgCaptor[ListBuffer[StreamImageExporter]]
    val image = ArgCaptor[RGBImage]
    verify(mockController).processRGBPhoto(image, filter, out)
  }

  test("--image test.png --output-console --output-file path"){
    view.processCommand(List("--image", "test.png", "--output-console"))
    verify(mockController).importImage("test.png")
    val filter = ArgCaptor[MixedFilter]
    val out = ArgCaptor[ListBuffer[StreamImageExporter]]
    val image = ArgCaptor[RGBImage]
    verify(mockController).processRGBPhoto(image, filter, out)
  }

  test("--image test.png --output-console --output-file"){
    view.processCommand(List("--image", "test.png", "--output-console", "--output-file"))
    verify(mockController).importImage("test.png")
    verify(mockController).showError("Some of arguments are not used, or not use but not properly:")
    verify(mockController).showError("\t--output-file")
  }

  test("--image-random --output-console --output-file path"){
    view.processCommand(List("--image-random", "--output-console", "--output-file", "path"))
    verify(mockController).importRandomImage()
    val filter = ArgCaptor[MixedFilter]
    val out = ArgCaptor[ListBuffer[StreamImageExporter]]
    val image = ArgCaptor[RGBImage]
    verify(mockController).processRGBPhoto(image, filter, out)
  }

  test("--image-random --output-console --output-file"){
    view.processCommand(List("--image-random", "--output-console", "--output-file"))
    verify(mockController).importRandomImage()
    verify(mockController).showError("Some of arguments are not used, or not use but not properly:")
    verify(mockController).showError("\t--output-file")
  }

  /** WITH FILTERS*/
  test("--image-random --output-console --output-file path --invert"){
    view.processCommand(List("--image-random", "--output-console", "--output-file" , "path", "--invert"))
    verify(mockController).importRandomImage()
    val filter = ArgCaptor[MixedFilter]
    val out = ArgCaptor[ListBuffer[StreamImageExporter]]
    val image = ArgCaptor[RGBImage]
    verify(mockController).processRGBPhoto(image, filter, out)
  }

  test("--image-random --output-console --output-file path --invert --brightness 1"){
    view.processCommand(List("--image-random", "--output-console", "--output-file" , "path", "--invert", "--brightness", "1"))
    verify(mockController).importRandomImage()
    val filter = ArgCaptor[MixedFilter]
    val out = ArgCaptor[ListBuffer[StreamImageExporter]]
    val image = ArgCaptor[RGBImage]
    verify(mockController).processRGBPhoto(image, filter, out)
  }

  test("--image-random --output-console --output-file path --invert --brightness 1 --flip x"){
    view.processCommand(List("--image-random", "--output-console", "--output-file" , "path", "--invert", "--brightness", "1", "--flip", "x"))
    verify(mockController).importRandomImage()
    val filter = ArgCaptor[MixedFilter]
    val out = ArgCaptor[ListBuffer[StreamImageExporter]]
    val image = ArgCaptor[RGBImage]
    verify(mockController).processRGBPhoto(image, filter, out)
  }

  test("--image-random --output-console --output-file path --invert --brightness 1 --flip y"){
    view.processCommand(List("--image-random", "--output-console", "--output-file" , "path", "--invert", "--brightness", "1", "--flip", "y"))
    verify(mockController).importRandomImage()
    val filter = ArgCaptor[MixedFilter]
    val out = ArgCaptor[ListBuffer[StreamImageExporter]]
    val image = ArgCaptor[RGBImage]
    verify(mockController).processRGBPhoto(image, filter, out)
  }

  test("--image-random --output-console --output-file path --invert --brightness -1 --flip x"){
    view.processCommand(List("--image-random", "--output-console", "--output-file" , "path", "--invert", "--brightness", "-1", "--flip", "x"))
    verify(mockController).importRandomImage()
    val filter = ArgCaptor[MixedFilter]
    val out = ArgCaptor[ListBuffer[StreamImageExporter]]
    val image = ArgCaptor[RGBImage]
    verify(mockController).processRGBPhoto(image, filter, out)
  }

  /**sucess test with check of size of filters, size of output endpoints and if we import valid image*/
  test("--image test.png --output-console --output-file path --invert --brightness -1 --flip y"){
    view.processCommand(List("--image","test.png", "--output-console", "--output-file" , "path", "--invert", "--brightness", "-1", "--flip", "y"))
    verify(mockController).importImage("test.png")
    val filter = ArgCaptor[MixedFilter]
    val out = ArgCaptor[ListBuffer[StreamImageExporter]]
    val image = ArgCaptor[RGBImage]
    assert(view.output.size == 2)
    assert(view.filter.size == 3)
    assert(view.image == img)
    verify(mockController).processRGBPhoto(image, filter, out)
  }

  /**sucess test with check of size of filters, size of output endpoints and if we import valid image*/
  test("HUGE filters --image test.png --output-console --output-file path --invert 5x --brightness -1 5x --flip  y"){
    view.processCommand(List("--image","test.png", "--output-console", "--output-file" , "path", "--invert" ,"--invert" ,"--invert" , "--invert" , "--invert", "--brightness", "-1", "--flip", "y","--flip", "y","--flip", "y","--flip", "y","--flip", "y"))
    verify(mockController).importImage("test.png")
    val filter = ArgCaptor[MixedFilter]
    val out = ArgCaptor[ListBuffer[StreamImageExporter]]
    val image = ArgCaptor[RGBImage]
    assert(view.output.size == 2)
    assert(view.filter.size == 11)
    assert(view.image == img)
    verify(mockController).processRGBPhoto(image, filter, out)
  }

  /**invalids*/

  test("--image-random --output-console --output-file path --invert --brightness --flip y"){
    view.processCommand(List("--image-random", "--output-console", "--output-file" , "path", "--invert", "--brightness", "--flip", "y"))
    verify(mockController).importRandomImage()
    verify(mockController).showError("Some of arguments are not used, or not use but not properly:")
    verify(mockController).showError("\t--brightness")
  }

  test("--image-random --output-console --output-file path --invert --brightness string"){
    view.processCommand(List("--image-random", "--output-console", "--output-file" , "path", "--invert", "--brightness", "string"))
    verify(mockController).importRandomImage()
    verify(mockController).showError("Some of arguments are not used, or not use but not properly:")
    verify(mockController).showError("\t--brightness")
    verify(mockController).showError("\tstring")
  }

  test("--image-random --output-console --output-file path --invert --brightness --flip"){
    view.processCommand(List("--image-random", "--output-console", "--output-file" , "path", "--invert", "--brightness", "--flip"))
    verify(mockController).importRandomImage()
    verify(mockController).showError("Some of arguments are not used, or not use but not properly:")
    verify(mockController).showError("\t--brightness")
    verify(mockController).showError("\t--flip")
  }

  test("--image-random --output-console --output-file path --invert --brightness --flip a"){
    view.processCommand(List("--image-random", "--output-console", "--output-file" , "path", "--invert", "--brightness", "--flip", "a"))
    verify(mockController).importRandomImage()
    verify(mockController).showError("Some of arguments are not used, or not use but not properly:")
    verify(mockController).showError("\t--brightness")
    verify(mockController).showError("\t--flip")
    verify(mockController).showError("\ta")
  }


}
