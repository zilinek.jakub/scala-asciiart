package console

import App.console.views.pages.concrete.HelpPage
import App.console.views.pages.generic.ErrorResponse
import org.scalatest.FunSuite

class PagesTest extends FunSuite{

  test("HelpPage"){
    assert((new HelpPage).render() ==
    "-----COMMANDS-----\n"
    +"Commands must be in order like: \n"
    +"1. Input source\n"
    +"\t--image $filepath - for specifying image to edit with path\n"
    +"\t--image-random - for specifying image to be randomly generated\n\n"
    +"2. List of image Correctors\n"
    +"\t--flip x - to flip image with X axis\n"
    +"\t--flip y - to flip image with X axis\n"
    +"\t--brightness 60 - to lower the brightness to 60%\n\n"
    +"3. Output\n"
    +"\t--output-console - to output result to console\n"
    +"\t--output-file $path - to output result to file at $path\n"
    )
  }
  test("ErrorPage"){
    assert((new ErrorResponse("error")).render() == "error\n")
    assert((new ErrorResponse("e\tr\nr\to\tr")).render() == "e\tr\nr\to\tr\n")
    assert((new ErrorResponse()).render() == "Error\n")
    assert((new ErrorResponse("")).render() == "\n")
  }

}
