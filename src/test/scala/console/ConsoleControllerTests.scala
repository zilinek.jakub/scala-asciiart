package console

import App.console.controllers.ConsoleController
import App.console.views.pages.concrete.HelpPage
import App.console.views.pages.generic.ErrorResponse
import App.exporters.Image.StreamImageExporter.StreamImageExporter
import App.exporters.Text.{StreamTextExporter, TextExporter}
import App.filters.charImage.mixed.MixedFilter
import App.filters.charImage.specific.{BrightnessFilter, FlipFilter, InvertFilter}
import App.models.Grid.Grid
import App.models.Image.RGBImage
import App.models.Pixel.RGB_Pixel
import org.scalatest.{BeforeAndAfterEach, FunSuite}

import java.io.ByteArrayOutputStream

class ConsoleControllerTests extends FunSuite with BeforeAndAfterEach{
  var exporter : TextExporter = _
  var stream : ByteArrayOutputStream = _
  var controller : ConsoleController = _

  override def beforeEach(): Unit = {
    stream = new ByteArrayOutputStream()
    exporter = new StreamTextExporter(stream)
    controller = new ConsoleController(exporter)
  }

  test("showHelp"){
    controller.showHelp()
    assert(stream.toString == new HelpPage().render()
    )
  }

  test("showError"){
    controller.showError("\n\n")
    assert(stream.toString == new ErrorResponse("\n\n").render()
    )
  }

  test("showError adv"){
    controller.showError("\ntest\n")
    assert(stream.toString == new ErrorResponse("\ntest\n").render()
    )
  }

  test("showError empty"){
    controller.showError("")
    assert(stream.toString == new ErrorResponse("").render()
    )
  }


  test("processRGBPhoto 1"){
    val img = RGBImage( Grid[RGB_Pixel](List(
      List(RGB_Pixel(List(255,255,255)),RGB_Pixel(List(255,255,255))),
      List(RGB_Pixel(List(255,255,255)),RGB_Pixel(List(255,255,255)))
    )))

    val buffer = new ByteArrayOutputStream()
    val buffer2 = new ByteArrayOutputStream()
    val out = List(StreamImageExporter(buffer), StreamImageExporter(buffer2))
    val filters = new MixedFilter(List(new InvertFilter(), new FlipFilter("x"), new BrightnessFilter(10)))

    controller.processRGBPhoto(img, filters, out )

    assert(buffer.toString == "@@\n@@\n")
    assert(buffer2.toString == "@@\n@@\n")
  }

  test("processRGBPhoto 2"){
    val img = RGBImage( Grid[RGB_Pixel](List(
      List(RGB_Pixel(List(0,0,0)),RGB_Pixel(List(0,0,0))),
      List(RGB_Pixel(List(0,0,0)),RGB_Pixel(List(0,0,0)))
    )))

    val buffer = new ByteArrayOutputStream()
    val buffer2 = new ByteArrayOutputStream()
    val out = List(StreamImageExporter(buffer), StreamImageExporter(buffer2) )
    val filters = new MixedFilter(
      List(new InvertFilter(), new FlipFilter("x"), new BrightnessFilter(10))
    )

    controller.processRGBPhoto(img, filters, out )

    assert(buffer.toString == "  \n  \n")
    assert(buffer2.toString == "  \n  \n")
  }

  test("processRGBPhoto 3"){
    val img = RGBImage( Grid[RGB_Pixel](List(
      List(RGB_Pixel(List(100,100,100)),RGB_Pixel(List(100,100,100))),
      List(RGB_Pixel(List(0,0,0)),RGB_Pixel(List(0,0,0)))
    )))

    val buffer = new ByteArrayOutputStream()
    val buffer2 = new ByteArrayOutputStream()
    val out = List(StreamImageExporter(buffer), StreamImageExporter(buffer2) )
    val filters = new MixedFilter(
      List(new InvertFilter(), new FlipFilter("x"), new BrightnessFilter(10))
    )

    controller.processRGBPhoto(img, filters, out )

    assert(buffer.toString == "  \n==\n")
    assert(buffer2.toString == "  \n==\n")
  }

}
