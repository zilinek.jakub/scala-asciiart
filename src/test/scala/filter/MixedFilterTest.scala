package filter

import App.filters.charImage.mixed.MixedFilter
import App.filters.charImage.specific.{BrightnessFilter, FlipFilter, InvertFilter}
import App.models.Grid.Grid
import App.models.Image.GrayImage
import App.models.Pixel.Gray_Pixel
import org.scalatest.FunSuite

class MixedFilterTest extends FunSuite{

  val img2x2: GrayImage = GrayImage(Grid[Gray_Pixel](List(
    List(Gray_Pixel(1),Gray_Pixel(2)),
    List(Gray_Pixel(3),Gray_Pixel(4)))))

  val img3x3: GrayImage = GrayImage(Grid[Gray_Pixel](List(
    List(Gray_Pixel(1),Gray_Pixel(2),Gray_Pixel(3)),
    List(Gray_Pixel(4),Gray_Pixel(5),Gray_Pixel(6)),
    List(Gray_Pixel(7),Gray_Pixel(8),Gray_Pixel(9)),
  )))
  /**
   * SINGLES
   * */
  test ("Brightness +") {
    var filter = new MixedFilter (List(
      new BrightnessFilter(10)
    ))
    val ref = GrayImage(Grid[Gray_Pixel](List(
      List(Gray_Pixel(11),Gray_Pixel(12)),
      List(Gray_Pixel(13),Gray_Pixel(14)))))
    assert(filter.filter(img2x2) == ref)
  }

  test ("Flip X") {
    var filter = new MixedFilter (List(
      new FlipFilter("x")
    ))
    val ref = GrayImage(Grid[Gray_Pixel](List(
      List(Gray_Pixel(3),Gray_Pixel(4)),
      List(Gray_Pixel(1),Gray_Pixel(2)))))
    assert(filter.filter(img2x2) == ref)
  }

  test ("Flip Y") {
    var filter = new MixedFilter (List(
      new FlipFilter("y")
    ))
    val ref = GrayImage(Grid[Gray_Pixel](List(
      List(Gray_Pixel(2),Gray_Pixel(1)),
      List(Gray_Pixel(4),Gray_Pixel(3)))))
    assert(filter.filter(img2x2) == ref)
  }

  test("Invert") {
    var filter = new MixedFilter (List(
      new InvertFilter()
    ))
    val ref = GrayImage(Grid[Gray_Pixel](List(
      List(Gray_Pixel(255-1),Gray_Pixel(255-2)),
      List(Gray_Pixel(255-3),Gray_Pixel(255-4)))))
    assert(filter.filter(img2x2) == ref)
  }

  /**
   * Doubles
   * */

  test ("Brightness - Flip") {
    var filter = new MixedFilter (List(
      new BrightnessFilter(10),
      new FlipFilter("x")
    ))
    val ref = GrayImage(Grid[Gray_Pixel](List(
      List(Gray_Pixel(13),Gray_Pixel(14)),
      List(Gray_Pixel(11),Gray_Pixel(12)))))
    assert(filter.filter(img2x2) == ref)
  }
  test ("Brightness + Flip") {
    var filter = new MixedFilter (List(
      new BrightnessFilter(-1),
      new FlipFilter("x")
    ))
    val ref = GrayImage(Grid[Gray_Pixel](List(
      List(Gray_Pixel(2),Gray_Pixel(3)),
      List(Gray_Pixel(0),Gray_Pixel(1)))))
    assert(filter.filter(img2x2) == ref)
  }

  test ("Brightness + Invert"){
    var filter = new MixedFilter (List(
      new BrightnessFilter(-1),
      new InvertFilter()
    ))
    val ref = GrayImage(Grid[Gray_Pixel](List(
      List(Gray_Pixel(255-0),Gray_Pixel(255-1)),
      List(Gray_Pixel(255-2),Gray_Pixel(255-3)))))

    assert(filter.filter(img2x2) == ref)
  }
  test ("Invert + Brightness"){
    var filter = new MixedFilter (List(
      new InvertFilter(),
      new BrightnessFilter(1)
    ))
    val ref = GrayImage(Grid[Gray_Pixel](List(
      List(Gray_Pixel(255-1+1),Gray_Pixel(255-2+1)),
      List(Gray_Pixel(255-3+1),Gray_Pixel(255-4+1)))))

    assert(filter.filter(img2x2) == ref)
  }

  test ("Invert + Flip"){
    var filter = new MixedFilter (List(
      new FlipFilter("x"),
      new InvertFilter()
    ))
    val ref = GrayImage(Grid[Gray_Pixel](List(
      List(Gray_Pixel(255-3),Gray_Pixel(255-4)),
      List(Gray_Pixel(255-1),Gray_Pixel(255-2))
    )))

    assert(filter.filter(img2x2) == ref)
  }
  test ("Flip x + Flip x"){
    var filter = new MixedFilter (List(
      new FlipFilter("x"),
      new FlipFilter("x")
    ))

    assert(filter.filter(img2x2) == img2x2)
  }
  test ("Flip y + Flip y"){
    var filter = new MixedFilter (List(
      new FlipFilter("y"),
      new FlipFilter("y")
    ))

    assert(filter.filter(img2x2) == img2x2)
  }
  test ("Flip y + Flip x"){
    var filter = new MixedFilter (List(
      new FlipFilter("x"),
      new FlipFilter("y")
    ))
    val ref = GrayImage(Grid[Gray_Pixel](List(
      List(Gray_Pixel(4),Gray_Pixel(3)),
      List(Gray_Pixel(2),Gray_Pixel(1))
    )))

    assert(filter.filter(img2x2) == ref)
  }


  /**
 * Triples
 * */
  test ("Brightness + Invert + Flip"){
    var filter = new MixedFilter (List(
      new BrightnessFilter(-1),
      new InvertFilter(),
      new FlipFilter("x")
    ))
    val ref = GrayImage(Grid[Gray_Pixel](List(
      List(Gray_Pixel(255-2),Gray_Pixel(255-3)),
      List(Gray_Pixel(255-0),Gray_Pixel(255-1))
    )))

    assert(filter.filter(img2x2) == ref)
  }
  test ("Invert + Brightness + Flip"){
    var filter = new MixedFilter (List(
      new InvertFilter(),
      new BrightnessFilter(1),
      new FlipFilter("x")
    ))
    val ref = GrayImage(Grid[Gray_Pixel](List(
      List(Gray_Pixel(255-3+1),Gray_Pixel(255-4+1)),
      List(Gray_Pixel(255-1+1),Gray_Pixel(255-2+1)))))

    assert(filter.filter(img2x2) == ref)
  }
  test ("Brightness + Invert + Brightness, check for overhead"){
    var filter = new MixedFilter (List(
      new BrightnessFilter(5),
      new InvertFilter(),
      new BrightnessFilter(100)
    ))
    val ref = GrayImage(Grid[Gray_Pixel](List(
      List(Gray_Pixel(255),Gray_Pixel(255)),
      List(Gray_Pixel(255),Gray_Pixel(255)))))

    assert(filter.filter(img2x2) == ref)
  }
}
