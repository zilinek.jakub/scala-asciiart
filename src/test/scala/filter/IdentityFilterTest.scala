package filter

import App.filters.defaults.IdentityFilter
import org.scalatest.FunSuite

class IdentityFilterTest extends FunSuite{
def filter(text: String) : String = new IdentityFilter[String].filter(text)

  test("Identity"){
    assert(filter("0") == "0")
    assert(filter("") == "")
    assert(filter("255") == "255")
  }

}
