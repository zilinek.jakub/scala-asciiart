package filter.specific

import App.filters.charImage.specific.BrightnessFilter
import App.models.Grid.Grid
import App.models.Image.GrayImage
import App.models.Pixel.Gray_Pixel
import org.scalatest.{BeforeAndAfterEach, FunSuite}

class BrightnessFilterTest extends FunSuite with BeforeAndAfterEach{

  test ("empty img +"){
    val filter = new BrightnessFilter(10)
    val img = GrayImage(Grid[Gray_Pixel](List(List())))
    val res = filter.filter(img)
    assert ( res == img )
  }

  test ("empty img -"){
    val filter = new BrightnessFilter(-10)
    val img = GrayImage(Grid[Gray_Pixel](List(List())))
    val res = filter.filter(img)
    assert ( res == img )
  }

  test("1x1"){
    val filter = new BrightnessFilter(10)
    val img = GrayImage(Grid[Gray_Pixel](List(List(Gray_Pixel(0)))))
    val ref = GrayImage(Grid[Gray_Pixel](List(List(Gray_Pixel(10)))))
    assert ( filter.filter(img) == ref )
  }

  test("1x1 overhead 1"){
    val filter = new BrightnessFilter(260)
    val img = GrayImage(Grid[Gray_Pixel](List(List(Gray_Pixel(0)))))
    val ref = GrayImage(Grid[Gray_Pixel](List(List(Gray_Pixel(255)))))
    assert ( filter.filter(img) == ref )
  }

  test("1x1 overhead 2"){
    val filter = new BrightnessFilter(16)
    val img = GrayImage(Grid[Gray_Pixel](List(List(Gray_Pixel(240)))))
    val ref = GrayImage(Grid[Gray_Pixel](List(List(Gray_Pixel(255)))))
    assert ( filter.filter(img) == ref )
  }

  test("1x1 subzero 1"){
    val filter = new BrightnessFilter(-100)
    val img = GrayImage(Grid[Gray_Pixel](List(List(Gray_Pixel(0)))))
    val ref = GrayImage(Grid[Gray_Pixel](List(List(Gray_Pixel(0)))))
    assert ( filter.filter(img) == ref )
  }

  test("1x1 subzero 2"){
    val filter = new BrightnessFilter(-100)
    val img = GrayImage(Grid[Gray_Pixel](List(List(Gray_Pixel(99)))))
    val ref = GrayImage(Grid[Gray_Pixel](List(List(Gray_Pixel(0)))))
    assert ( filter.filter(img) == ref )
  }

  test("1x1 zero addition"){
    val filter = new BrightnessFilter(0)
    val ref = GrayImage(Grid[Gray_Pixel](List(List(Gray_Pixel(0)))))
    val img = GrayImage(Grid[Gray_Pixel](List(List(Gray_Pixel(0)))))
    assert ( filter.filter(img) == ref )
  }

  test("2x1 +"){
    val filter = new BrightnessFilter(10)
    val img = GrayImage(Grid[Gray_Pixel](List(List(Gray_Pixel(100), Gray_Pixel(90)))))
    val ref = GrayImage(Grid[Gray_Pixel](List(List(Gray_Pixel(110), Gray_Pixel(100)))))
    assert ( filter.filter(img) == ref )
  }

  test("2x1 -"){
    val filter = new BrightnessFilter(-10)
    val img = GrayImage(Grid[Gray_Pixel](List(List(Gray_Pixel(100), Gray_Pixel(90)))))
    val ref = GrayImage(Grid[Gray_Pixel](List(List(Gray_Pixel(90), Gray_Pixel(80)))))
    assert ( filter.filter(img) == ref )
  }

  test("1x2 +"){
    val filter = new BrightnessFilter(10)
    val img = GrayImage(Grid[Gray_Pixel](List(List(Gray_Pixel(100)),List(Gray_Pixel(90)))))
    val ref = GrayImage(Grid[Gray_Pixel](List(List(Gray_Pixel(110)),List(Gray_Pixel(100)))))
    assert ( filter.filter(img) == ref )
  }

  test("1x2 -"){
    val filter = new BrightnessFilter(-10)
    val img = GrayImage(Grid[Gray_Pixel](List(List(Gray_Pixel(100)),List(Gray_Pixel(100)))))
    val ref = GrayImage(Grid[Gray_Pixel](List(List(Gray_Pixel(90)),List(Gray_Pixel(90)))))
    assert ( filter.filter(img) == ref )
  }


  test("2x2 +"){
    val filter = new BrightnessFilter(10)
    val img = GrayImage(Grid[Gray_Pixel](List(List(Gray_Pixel(100),Gray_Pixel(100)),
      List(Gray_Pixel(100),Gray_Pixel(100)))))

    val ref = GrayImage(Grid[Gray_Pixel](List(List(Gray_Pixel(110),Gray_Pixel(110)),
      List(Gray_Pixel(110),Gray_Pixel(110)))))
    assert ( filter.filter(img) == ref )
  }

  test("2x2 0 -"){
    val filter = new BrightnessFilter(-10)
    val img = GrayImage(Grid[Gray_Pixel](List(List(Gray_Pixel(100),Gray_Pixel(100)),
      List(Gray_Pixel(100),Gray_Pixel(100)))))

    val ref = GrayImage(Grid[Gray_Pixel](List(List(Gray_Pixel(90),Gray_Pixel(90)),
      List(Gray_Pixel(90),Gray_Pixel(90)))))

    assert ( filter.filter(img) == ref )
  }
}
