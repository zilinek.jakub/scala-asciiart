package filter.specific

import App.filters.charImage.specific.FlipFilter
import App.models.Grid.Grid
import App.models.Image.GrayImage
import App.models.Pixel.Gray_Pixel
import org.scalatest.{BeforeAndAfterEach, FunSuite}

class FlipFilterTests extends FunSuite with BeforeAndAfterEach{

  var filterX : FlipFilter = _
  var filterY : FlipFilter = _

  override def beforeEach(): Unit = {
    this.filterX = new FlipFilter("x")
    this.filterY = new FlipFilter("y")
  }


  test ("[X]empty img"){
    val img = GrayImage(Grid[Gray_Pixel](List(List())))
    val res = filterX.filter(img)
    assert ( res == img )
  }

  test ("[Y]empty img"){
    val img = GrayImage(Grid[Gray_Pixel](List(List())))
    val res = filterX.filter(img)
    assert ( res == img )
  }

  test("[X]1x1"){
    val img = GrayImage(Grid[Gray_Pixel](List(List(Gray_Pixel(0)))))
    val ref = GrayImage(Grid[Gray_Pixel](List(List(Gray_Pixel(0)))))
    assert ( filterX.filter(img) == ref )
  }

  test("[Y]1x1"){
    val ref = GrayImage(Grid[Gray_Pixel](List(List(Gray_Pixel(0)))))
    val img = GrayImage(Grid[Gray_Pixel](List(List(Gray_Pixel(0)))))
    assert ( filterY.filter(img) == ref )
  }

  test("[X]2x1"){
    val img = GrayImage(Grid[Gray_Pixel](List(List(Gray_Pixel(0)),List(Gray_Pixel(1)))))
    val ref = GrayImage(Grid[Gray_Pixel](List(List(Gray_Pixel(1)),List(Gray_Pixel(0)))))
    assert ( filterX.filter(img) == ref )
  }

  test("[Y]2x1"){
    val ref = GrayImage(Grid[Gray_Pixel](List(List(Gray_Pixel(0)),List(Gray_Pixel(1)))))
    val img = GrayImage(Grid[Gray_Pixel](List(List(Gray_Pixel(0)),List(Gray_Pixel(1)))))
    assert ( filterY.filter(img) == ref )
  }

  test("[X]1x2"){
    val img = GrayImage(Grid[Gray_Pixel](List(List(Gray_Pixel(0), Gray_Pixel(1)))))
    val ref = GrayImage(Grid[Gray_Pixel](List(List(Gray_Pixel(0), Gray_Pixel(1)))))
    assert ( filterX.filter(img) == ref )
  }

  test("[Y]1x2"){
    val img = GrayImage(Grid[Gray_Pixel](List(List(Gray_Pixel(0), Gray_Pixel(1)))))
    val ref = GrayImage(Grid[Gray_Pixel](List(List(Gray_Pixel(1), Gray_Pixel(0)))))
    assert ( filterY.filter(img) == ref )
  }


  test("[X]2x2"){
    val img = GrayImage(Grid[Gray_Pixel](List(List(Gray_Pixel(0),Gray_Pixel(1)),
      List(Gray_Pixel(2),Gray_Pixel(3)))))

    val ref = GrayImage(Grid[Gray_Pixel](List(List(Gray_Pixel(2),Gray_Pixel(3)),
      List(Gray_Pixel(0),Gray_Pixel(1)))))
    assert ( filterX.filter(img) == ref )
  }

  test("[Y]2x2"){
    val img = GrayImage(Grid[Gray_Pixel](List(
      List(Gray_Pixel(0),Gray_Pixel(1)),
      List(Gray_Pixel(2),Gray_Pixel(3))
    )))

    val ref = GrayImage(Grid[Gray_Pixel](List(List(Gray_Pixel(1),Gray_Pixel(0)),
      List(Gray_Pixel(3),Gray_Pixel(2)))))

    assert ( filterY.filter(img) == ref )
  }

  test("[Y] 3x3"){
    val img = GrayImage(Grid[Gray_Pixel](List(
      List(Gray_Pixel(1),Gray_Pixel(2),Gray_Pixel(3)),
      List(Gray_Pixel(4),Gray_Pixel(5),Gray_Pixel(6)),
      List(Gray_Pixel(7),Gray_Pixel(8),Gray_Pixel(9)),
    )))

    val ref = GrayImage(Grid[Gray_Pixel](List(
      List(Gray_Pixel(3),Gray_Pixel(2),Gray_Pixel(1)),
      List(Gray_Pixel(6),Gray_Pixel(5),Gray_Pixel(4)),
      List(Gray_Pixel(9),Gray_Pixel(8),Gray_Pixel(7)),
    )))

    assert ( filterY.filter(img) == ref )
  }

  test("[X] 3x3"){
    val img = GrayImage(Grid[Gray_Pixel](List(
      List(Gray_Pixel(1),Gray_Pixel(2),Gray_Pixel(3)),
      List(Gray_Pixel(4),Gray_Pixel(5),Gray_Pixel(6)),
      List(Gray_Pixel(7),Gray_Pixel(8),Gray_Pixel(9)),
    )))

    val ref = GrayImage(Grid[Gray_Pixel](List(
      List(Gray_Pixel(7),Gray_Pixel(8),Gray_Pixel(9)),
      List(Gray_Pixel(4),Gray_Pixel(5),Gray_Pixel(6)),
      List(Gray_Pixel(1),Gray_Pixel(2),Gray_Pixel(3)),
    )))

    assert ( filterX.filter(img) == ref )
  }
}
