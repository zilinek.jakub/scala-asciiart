package filter.specific

import App.filters.charImage.specific.InvertFilter
import App.models.Grid.Grid
import App.models.Image.GrayImage
import App.models.Pixel.Gray_Pixel
import org.scalatest.{BeforeAndAfterEach, FunSuite}


class InvertFilterTest extends FunSuite with BeforeAndAfterEach{

  var filter : InvertFilter = _

  override def beforeEach(): Unit = {
    this.filter = new InvertFilter()
  }

  test ("empty img"){
    val img = GrayImage(Grid[Gray_Pixel](List(List())))
    val res = filter.filter(img)
    assert ( res == img )
  }

  test("1x1 0 -> 255"){
    val img = GrayImage(Grid[Gray_Pixel](List(List(Gray_Pixel(0)))))
    val ref = GrayImage(Grid[Gray_Pixel](List(List(Gray_Pixel(255)))))
    assert ( filter.filter(img) == ref )
  }

  test("1x1 255 -> 0"){
    val ref = GrayImage(Grid[Gray_Pixel](List(List(Gray_Pixel(0)))))
    val img = GrayImage(Grid[Gray_Pixel](List(List(Gray_Pixel(255)))))
    assert ( filter.filter(img) == ref )
  }

  test("2x1 0 -> 255"){
    val img = GrayImage(Grid[Gray_Pixel](List(List(Gray_Pixel(0)),List(Gray_Pixel(0)))))
    val ref = GrayImage(Grid[Gray_Pixel](List(List(Gray_Pixel(255)),List(Gray_Pixel(255)))))
    assert ( filter.filter(img) == ref )
  }

  test("2x1 255 -> 0"){
    val ref = GrayImage(Grid[Gray_Pixel](List(List(Gray_Pixel(0)),List(Gray_Pixel(0)))))
    val img = GrayImage(Grid[Gray_Pixel](List(List(Gray_Pixel(255)),List(Gray_Pixel(255)))))
    assert ( filter.filter(img) == ref )
  }

  test("2x2 0 -> 255"){
    val img = GrayImage(Grid[Gray_Pixel](List(List(Gray_Pixel(0),Gray_Pixel(0)),
      List(Gray_Pixel(0),Gray_Pixel(0)))))
    val ref = GrayImage(Grid[Gray_Pixel](List(List(Gray_Pixel(255),Gray_Pixel(255)),
      List(Gray_Pixel(255),Gray_Pixel(255)))))
    assert ( filter.filter(img) == ref )
  }

  test("2x2 random -> random"){
    val img = GrayImage(Grid[Gray_Pixel](List(List(Gray_Pixel(100),Gray_Pixel(55)),
      List(Gray_Pixel(12),Gray_Pixel(45)))))
    val ref = GrayImage(Grid[Gray_Pixel](List(List(Gray_Pixel(255-100),Gray_Pixel(255-55)),
      List(Gray_Pixel(255-12),Gray_Pixel(255-45)))))
    assert ( filter.filter(img) == ref )
  }

}
