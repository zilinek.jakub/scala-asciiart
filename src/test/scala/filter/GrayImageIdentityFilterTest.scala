package filter

import App.filters.charImage.defaults.GrayImageIdentityFilter
import App.models.Grid.Grid
import App.models.Image.GrayImage
import App.models.Pixel.Gray_Pixel
import org.scalatest.FunSuite

class GrayImageIdentityFilterTest extends FunSuite{
  def filter(item: GrayImage) : GrayImage = GrayImageIdentityFilter.filter(item)

  test("Identity"){
    assert(filter(GrayImage(Grid[Gray_Pixel](List(List())))) == GrayImage(Grid[Gray_Pixel](List(List()))))
    assert(filter(GrayImage(Grid[Gray_Pixel](List(List(Gray_Pixel(2)))))) == GrayImage(Grid[Gray_Pixel](List(List(Gray_Pixel(2))))))
    assert(filter(GrayImage(Grid[Gray_Pixel](List(List(Gray_Pixel(-2)))))) == GrayImage(Grid[Gray_Pixel](List(List(Gray_Pixel(-2))))))
    assert(filter(
      GrayImage(Grid[Gray_Pixel](List(List(Gray_Pixel(2),Gray_Pixel(2)),List(Gray_Pixel(2),Gray_Pixel(2)))))) ==
      GrayImage(Grid[Gray_Pixel](List(List(Gray_Pixel(2),Gray_Pixel(2)),List(Gray_Pixel(2),Gray_Pixel(2))))))
  }
}
