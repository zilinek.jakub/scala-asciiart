package convertors

import App.converters.Gray2ASCIIChar
import App.models.Grid.Grid
import App.models.Image.{CharImage, GrayImage}
import App.models.Pixel.{Char_Pixel, Gray_Pixel}
import org.scalatest.FunSuite

class Gray2ASCIICharTest extends FunSuite{

  test("empty string will not crash"){
    val safe = Gray2ASCIIChar._ASCIIString
    Gray2ASCIIChar._ASCIIString = ""
    for (i <- 0 until 255)
      assert(Gray2ASCIIChar.convert(i) == ' ')
    Gray2ASCIIChar._ASCIIString = safe
  }

  test("edge cases"){
    val size = Gray2ASCIIChar._ASCIIString.length - 1
    assert(Gray2ASCIIChar.convert(0) == Gray2ASCIIChar._ASCIIString.charAt(0))
    assert(Gray2ASCIIChar.convert(255) == Gray2ASCIIChar._ASCIIString.charAt(((size * 255/255).floor.toInt)))
    assert(Gray2ASCIIChar.convert(110) == Gray2ASCIIChar._ASCIIString.charAt(((size * 110/255).floor.toInt)))
    assert(Gray2ASCIIChar.convert(10) == Gray2ASCIIChar._ASCIIString.charAt(((size * 10/255).floor.toInt)))
  }

  test("edge cases with space string"){
    val safe = Gray2ASCIIChar._ASCIIString
    Gray2ASCIIChar._ASCIIString = " "

    val size = Gray2ASCIIChar._ASCIIString.length - 1
    assert(Gray2ASCIIChar.convert(0) == Gray2ASCIIChar._ASCIIString.charAt(0))
    assert(Gray2ASCIIChar.convert(255) == Gray2ASCIIChar._ASCIIString.charAt(((size * 255/255).floor.toInt)))
    assert(Gray2ASCIIChar.convert(110) == Gray2ASCIIChar._ASCIIString.charAt(((size * 110/255).floor.toInt)))
    assert(Gray2ASCIIChar.convert(10) == Gray2ASCIIChar._ASCIIString.charAt(((size * 10/255).floor.toInt)))
    Gray2ASCIIChar._ASCIIString = safe
  }

}
