package convertors

import App.converters.{Gray2ASCIIChar, Gray2CharImage}
import App.models.Grid.Grid
import App.models.Image.{CharImage, GrayImage}
import App.models.Pixel.{Char_Pixel, Gray_Pixel}
import org.scalatest.FunSuite

class Gray2CharImageTest extends FunSuite{

    val convertor: Gray2CharImage.type = Gray2CharImage

    def charAtASCIIString(int: Char) : Char = {
      Gray2ASCIIChar.convert(int)
    }

    test("empty"){
      val test = GrayImage(Grid[Gray_Pixel](List(List())))
      convertor.convert(test)
      assert ( convertor.convert(test) == CharImage(Grid[Char_Pixel](List(List()))) )
    }
    test ("random"){
      val img: GrayImage = GrayImage(Grid[Gray_Pixel](List(
        List(Gray_Pixel(141),Gray_Pixel(141),Gray_Pixel(29)),
        List(Gray_Pixel(141),Gray_Pixel(141),Gray_Pixel(10)),
        List(Gray_Pixel(141),Gray_Pixel(141),Gray_Pixel(141))
      )))

      val ref: CharImage = CharImage(Grid[Char_Pixel](List(
        List(Char_Pixel(charAtASCIIString(141)),Char_Pixel(charAtASCIIString(141)),Char_Pixel(charAtASCIIString(29))),
        List(Char_Pixel(charAtASCIIString(141)),Char_Pixel(charAtASCIIString(141)),Char_Pixel(charAtASCIIString(10))),
        List(Char_Pixel(charAtASCIIString(141)),Char_Pixel(charAtASCIIString(141)),Char_Pixel(charAtASCIIString(141)))
      )))

      assert ( convertor.convert(img) == ref )
    }
    test ("255") {
        val img: GrayImage = GrayImage(Grid[Gray_Pixel](List(
          List(Gray_Pixel(255), Gray_Pixel(255), Gray_Pixel(255)),
          List(Gray_Pixel(255), Gray_Pixel(255), Gray_Pixel(255)),
          List(Gray_Pixel(255), Gray_Pixel(255), Gray_Pixel(255))
        )))

        val ref: CharImage = CharImage(Grid[Char_Pixel](List(
          List(Char_Pixel(charAtASCIIString(255)),Char_Pixel(charAtASCIIString(255)),Char_Pixel(charAtASCIIString(255))),
          List(Char_Pixel(charAtASCIIString(255)),Char_Pixel(charAtASCIIString(255)),Char_Pixel(charAtASCIIString(255))),
          List(Char_Pixel(charAtASCIIString(255)),Char_Pixel(charAtASCIIString(255)),Char_Pixel(charAtASCIIString(255)))
        )))

        assert ( convertor.convert(img) == ref )
      }
    test ("0") {
      val img: GrayImage = GrayImage(Grid[Gray_Pixel](List(
        List(Gray_Pixel(0), Gray_Pixel(0), Gray_Pixel(0)),
        List(Gray_Pixel(0), Gray_Pixel(0), Gray_Pixel(0)),
        List(Gray_Pixel(0), Gray_Pixel(0), Gray_Pixel(0))
      )))

      val ref: CharImage = CharImage(Grid[Char_Pixel](List(
        List(Char_Pixel(charAtASCIIString(0)),Char_Pixel(charAtASCIIString(0)),Char_Pixel(charAtASCIIString(0))),
        List(Char_Pixel(charAtASCIIString(0)),Char_Pixel(charAtASCIIString(0)),Char_Pixel(charAtASCIIString(0))),
        List(Char_Pixel(charAtASCIIString(0)),Char_Pixel(charAtASCIIString(0)),Char_Pixel(charAtASCIIString(0)))
      )))

      assert ( convertor.convert(img) == ref )
    }
    test("change ascii string"){
    val img: GrayImage = GrayImage(Grid[Gray_Pixel](List(
      List(Gray_Pixel(25),Gray_Pixel(50),Gray_Pixel(75)),
      List(Gray_Pixel(100),Gray_Pixel(125),Gray_Pixel(150)),
      List(Gray_Pixel(175),Gray_Pixel(200),Gray_Pixel(225))
    )))

    val ref: CharImage = CharImage(Grid[Char_Pixel](List(
      List(Char_Pixel(charAtASCIIString(25)),Char_Pixel(charAtASCIIString(50)),Char_Pixel(charAtASCIIString(75))),
      List(Char_Pixel(charAtASCIIString(100)),Char_Pixel(charAtASCIIString(125)),Char_Pixel(charAtASCIIString(150))),
      List(Char_Pixel(charAtASCIIString(175)),Char_Pixel(charAtASCIIString(200)),Char_Pixel(charAtASCIIString(225)))
    )))

    val res = convertor.convert(img)
    assert ( res == ref )
    val safe = Gray2ASCIIChar._ASCIIString
    Gray2ASCIIChar._ASCIIString = " "

    val ref2: CharImage = CharImage(Grid[Char_Pixel](List(
      List(Char_Pixel(charAtASCIIString(25)),Char_Pixel(charAtASCIIString(50)),Char_Pixel(charAtASCIIString(75))),
      List(Char_Pixel(charAtASCIIString(100)),Char_Pixel(charAtASCIIString(125)),Char_Pixel(charAtASCIIString(150))),
      List(Char_Pixel(charAtASCIIString(175)),Char_Pixel(charAtASCIIString(200)),Char_Pixel(charAtASCIIString(225)))
    )))

    val res2 = convertor.convert(img)
    assert ( res == ref )
    assert ( res != res2 )
      Gray2ASCIIChar._ASCIIString = safe
  }
}
