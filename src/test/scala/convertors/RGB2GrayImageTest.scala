package convertors

import App.converters.RGB2GrayImage
import App.models.Grid.Grid
import App.models.Image.{GrayImage, RGBImage}
import App.models.Pixel.{Gray_Pixel, RGB_Pixel}
import org.scalatest.FunSuite

class RGB2GrayImageTest extends FunSuite{

  val convertor: RGB2GrayImage.type = RGB2GrayImage


  test("empty"){
    val test = RGBImage(Grid[RGB_Pixel](List(List())))
    convertor.convert(test)
    assert ( convertor.convert(test) == GrayImage(Grid[Gray_Pixel](List(List()))) )
  }
  test ("random"){
    val img: RGBImage = RGBImage(Grid[RGB_Pixel](List(
      List(RGB_Pixel(List(100,150,200)),RGB_Pixel(List(100,150,200)),RGB_Pixel(List(0,1,255))),
      List(RGB_Pixel(List(100,150,200)),RGB_Pixel(List(100,150,200)),RGB_Pixel(List(10,10,10))),
      List(RGB_Pixel(List(100,150,200)),RGB_Pixel(List(100,150,200)),RGB_Pixel(List(100,150,200))),
    )))

    assert(convertor.convert(img) == GrayImage(Grid[Gray_Pixel](List(
      List(Gray_Pixel(140),Gray_Pixel(140),Gray_Pixel(28)),
      List(Gray_Pixel(140),Gray_Pixel(140),Gray_Pixel(9)),
      List(Gray_Pixel(140),Gray_Pixel(140),Gray_Pixel(140))
    ))))
  }
  test ("255") {
    val img: RGBImage = RGBImage(Grid[RGB_Pixel](List(
      List(RGB_Pixel(List(255, 255, 255)), RGB_Pixel(List(255, 255, 255)), RGB_Pixel(List(255, 255, 255))),
      List(RGB_Pixel(List(255, 255, 255)), RGB_Pixel(List(255, 255, 255)), RGB_Pixel(List(255, 255, 255))),
      List(RGB_Pixel(List(255, 255, 255)), RGB_Pixel(List(255, 255, 255)), RGB_Pixel(List(255, 255, 255))),
    )))

    assert(convertor.convert(img) == GrayImage(Grid[Gray_Pixel](List(
      List(Gray_Pixel(255), Gray_Pixel(255), Gray_Pixel(255)),
      List(Gray_Pixel(255), Gray_Pixel(255), Gray_Pixel(255)),
      List(Gray_Pixel(255), Gray_Pixel(255), Gray_Pixel(255))
    ))))
  }
  test ("0") {
    val img: RGBImage = RGBImage(Grid[RGB_Pixel](List(
      List(RGB_Pixel(List(0, 0, 0)), RGB_Pixel(List(0, 0, 0)), RGB_Pixel(List(0, 0, 0))),
      List(RGB_Pixel(List(0, 0, 0)), RGB_Pixel(List(0, 0, 0)), RGB_Pixel(List(0, 0, 0))),
      List(RGB_Pixel(List(0, 0, 0)), RGB_Pixel(List(0, 0, 0)), RGB_Pixel(List(0, 0, 0))),
    )))

    assert(convertor.convert(img) == GrayImage(Grid[Gray_Pixel](List(
      List(Gray_Pixel(0), Gray_Pixel(0), Gray_Pixel(0)),
      List(Gray_Pixel(0), Gray_Pixel(0), Gray_Pixel(0)),
      List(Gray_Pixel(0), Gray_Pixel(0), Gray_Pixel(0))
    ))))
  }
  test ("invalid RGBPixel check from negative to 0") {
    val img: RGBImage = RGBImage(Grid[RGB_Pixel](List(
      List(RGB_Pixel(List(-100, -100, -100)), RGB_Pixel(List(0, 0, 0)), RGB_Pixel(List(0, 0, 0))),
      List(RGB_Pixel(List(0, 0, 0)), RGB_Pixel(List(0, 0, 0)), RGB_Pixel(List(0, 0, 0))),
      List(RGB_Pixel(List(0, 0, 0)), RGB_Pixel(List(0, 0, 0)), RGB_Pixel(List(0, 0, 0))),
    )))

    assert(convertor.convert(img) == GrayImage(Grid[Gray_Pixel](List(
      List(Gray_Pixel(0), Gray_Pixel(0), Gray_Pixel(0)),
      List(Gray_Pixel(0), Gray_Pixel(0), Gray_Pixel(0)),
      List(Gray_Pixel(0), Gray_Pixel(0), Gray_Pixel(0))
    ))))
  }
}
