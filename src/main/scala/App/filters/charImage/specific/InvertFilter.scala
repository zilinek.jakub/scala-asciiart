package App.filters.charImage.specific

import App.filters.charImage.GrayImageFilter
import App.models.Grid.Grid
import App.models.Image.GrayImage
import App.models.Pixel.Gray_Pixel

import scala.collection.mutable.ListBuffer


/**
 * Filters all pixels in image
 * Inverts the brightness of pixels in image
 * */
class InvertFilter extends GrayImageFilter{
  var _max = 255
  /**
   * Inverts the brightness of pixels in image
   * @param item is GrayImage that will be inverted
   * */
  override def filter(item: GrayImage): GrayImage ={
    val res : ListBuffer[ListBuffer[Gray_Pixel]] = ListBuffer()

    for (row <- item.grid.pixels){
      val tmp : ListBuffer[Gray_Pixel] = ListBuffer()
      for (pix <- row){
        tmp.addOne(Gray_Pixel( _max - pix.get() ))
      }
      res.addOne(tmp)
    }

    GrayImage(Grid[Gray_Pixel](res))
  }
}
