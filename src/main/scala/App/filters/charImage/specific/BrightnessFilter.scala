package App.filters.charImage.specific

import App.filters.charImage.GrayImageFilter
import App.models.Grid.Grid
import App.models.Image.GrayImage
import App.models.Pixel.Gray_Pixel

import scala.collection.mutable.ListBuffer


/**
 * BrightnessFilter adds some value to all Pixels of image
 *
 * @constructor value is value that will be added to all pixels in GrayImage passed in filter method
 * */
class BrightnessFilter(value : Int) extends GrayImageFilter{

  /**
   * Adds value to all pixels in image passed as item paramter
   * @param item is image that will be filtered
   * @note if value of new pixel is sub 0 it will be limited to 0 or if more than 255 it will be set as 255
   * @return filtered GrayImage
   * */
  override def filter(item: GrayImage): GrayImage ={
    val res : ListBuffer[ListBuffer[Gray_Pixel]] = ListBuffer()

    for (row <- item.grid.pixels){
      val tmp : ListBuffer[Gray_Pixel] = ListBuffer()
      for (pix <- row){
        var newValue = pix.get() + value
        if (newValue < 0)
          newValue = 0
        if (newValue > 255)
          newValue = 255
        tmp.addOne(Gray_Pixel( newValue ))
      }
      res.addOne(tmp)
    }

    GrayImage(Grid[Gray_Pixel](res))
  }
}
