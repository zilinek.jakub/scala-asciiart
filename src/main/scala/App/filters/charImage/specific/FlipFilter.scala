package App.filters.charImage.specific

import App.filters.charImage.GrayImageFilter
import App.models.Grid.Grid
import App.models.Image.GrayImage
import App.models.Pixel.Gray_Pixel

import scala.collection.mutable.ListBuffer

/**
 * Flips image by some axes, this axes is specified in constructor
 * @constructor axes is axis that the image will be fliped by, must be X or Y otherwise ignored
 * */
class FlipFilter(axes: String) extends GrayImageFilter{
  /**
   * Filter image passed in parameter by axis X
   * */
  private def AxisX(item: GrayImage): GrayImage = {
    val res : ListBuffer[ListBuffer[Gray_Pixel]] = ListBuffer()
    for (it <- item.grid.pixels) {
      res.insert(0, ListBuffer.empty ++= it.toList)
    }
    GrayImage(Grid[Gray_Pixel](res))
  }
  /**
   * Filter image passed in parameter by axis Y
   * */
  private def AxisY(item: GrayImage): GrayImage = {
    val res : ListBuffer[ListBuffer[Gray_Pixel]] = ListBuffer()
    for (it <- item.grid.pixels) {
      val row = new ListBuffer[Gray_Pixel]
      for (it2 <- it) {
        row.insert(0, it2)
      }
      res += row
    }
    GrayImage(Grid[Gray_Pixel](res))
  }

  /**
   * Filter image passed in parameter by axis X
   * */
  override def filter(item: GrayImage): GrayImage = {
    if (axes == "x")
      return AxisX(item)
    if (axes == "y")
      AxisY(item)
    else
      item

  }
}
