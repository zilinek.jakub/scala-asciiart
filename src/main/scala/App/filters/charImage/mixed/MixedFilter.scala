package App.filters.charImage.mixed

import App.filters.charImage.GrayImageFilter
import App.models.Image.GrayImage

/**
 * Represents Mixed filter that containes multiple filters and will apply them all on image passed in filter method
 * */
class MixedFilter(filters: Seq[GrayImageFilter]) extends GrayImageFilter {

  /**
   * @param item - on this image we apply all filters set in constructor parameter
   * */
  override def filter(item: GrayImage): GrayImage =
    filters.foldLeft(item)((accumulator, filter) => filter.filter(accumulator))

  /**
   * return how many filters we will apply to Image
   * */
  def size : Int = filters.size
}
