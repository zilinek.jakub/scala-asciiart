package App.filters.charImage.defaults

import App.filters.charImage.GrayImageFilter
import App.filters.defaults.IdentityFilter
import App.models.Image.GrayImage

object GrayImageIdentityFilter
  extends IdentityFilter[GrayImage] with GrayImageFilter
{

}
