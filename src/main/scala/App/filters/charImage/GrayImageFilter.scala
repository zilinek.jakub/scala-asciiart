package App.filters.charImage

import App.filters.Filter
import App.models.Image.GrayImage

/**
 * Filters for GrayImage
 * */
trait GrayImageFilter extends Filter[GrayImage]
{

}
