package App.filters

trait Filter[T]
{
  /**
   * Corrects an item
   * @param item The item to correct
   * @return The corrected item
   */
  def filter(item: T): T
}
