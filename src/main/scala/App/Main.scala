package App

import App.console.controllers.ConsoleController
import App.console.views.ConsoleView
import App.exporters.Text.StdOutExporter



object Main extends App {

  // init components
  val stdout = new StdOutExporter
  val controller = new ConsoleController(stdout)
  val consoleView = new ConsoleView(controller)

  // run view
  consoleView.processCommand(args.toList)
}
