package App.importers.ImageImporter

import App.importers.Importer
import App.models.Image.Image
import App.models.Pixel.Pixel

/** @tparam OUT has boundary for Image with Pixel
 *
 * */
trait ImageImporter[OUT <: Image[Pixel]] extends Importer[OUT] {

}
