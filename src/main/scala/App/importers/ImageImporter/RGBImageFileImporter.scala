package App.importers.ImageImporter
import App.models.Grid.Grid
import App.models.Image.RGBImage
import App.models.Pixel.RGB_Pixel

import java.awt.Color
import java.awt.image.BufferedImage
import java.io.File
import javax.imageio.ImageIO
import scala.collection.mutable.ArrayBuffer

case class RGBImageFileImporter(path: String) extends ImageImporter[RGBImage]{
  /** imports item with @param path
   * @return Option of ImageRGB, if success returns object, otherwise empty
   * */
  override def importItem(): Option[RGBImage] = {

    var img : BufferedImage = null
    try {
      img = ImageIO.read(new File(this.path))
      var height = img.getHeight() - 1
      var width = img.getWidth() - 1
      var collumn : ArrayBuffer[ArrayBuffer[RGB_Pixel]] = ArrayBuffer()
      for (h <- 0 to height){
        var row : ArrayBuffer[RGB_Pixel] = ArrayBuffer()
        for (w <- 0 to width){
          var col = new Color(img.getRGB(w, h))
          row.addOne(RGB_Pixel(List(col.getRed,col.getGreen, col.getBlue)))
        }
        collumn.addOne(row)
      }
      Option(RGBImage(Grid[RGB_Pixel](collumn)))
    } catch {
      case _ : Throwable =>  Option.empty
    }
  }
}
