package App.importers.ImageImporter

import App.models.Grid.Grid
import App.models.Image.RGBImage
import App.models.Pixel.RGB_Pixel

import scala.collection.mutable.ArrayBuffer
import scala.math.abs
import scala.util.Random

/**
 * @param width specifies width of created image
 * @param height specifies height of created image
 * @note if negative value is inserted in some parameter it will be converted to absolute value =: non negative value =: -100 -> 100
 * */
case class RandomImageImporter (width: Int, height: Int) extends ImageImporter[RGBImage]{
  /**
   * Import something somewhere
   */
  override def importItem(): Option[RGBImage] = {
    var collumn : ArrayBuffer[ArrayBuffer[RGB_Pixel]] = ArrayBuffer()
    for (h <- 0 until abs(height)){
      var row : ArrayBuffer[RGB_Pixel] = ArrayBuffer()
      for (w <- 0 until abs(width)){
        row.addOne(RGB_Pixel(List(abs(Random.nextInt() % 255),abs(Random.nextInt() % 255), abs(Random.nextInt() % 255))))
      }
      collumn.addOne(row)
    }
    Option(RGBImage(Grid[RGB_Pixel](collumn)))
  }
}
