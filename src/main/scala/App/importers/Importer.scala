package App.importers


/** @tparam OUT output type
 *  @tparam IN represents from what type will be OUT loaded
 * */
trait Importer[OUT] {

  /**
   * Import something somewhere
   */
  def importItem(): Option[OUT]

}
