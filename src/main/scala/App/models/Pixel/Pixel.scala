package App.models.Pixel


/**
 * abstract representation of Pixel
 * */
abstract class Pixel {

  /**
   * get gets raw value stored in Pixel that is created in constructor
   * */
  def get() : Any
  /**
   * Converts values stored in Pixel to Array[Char]
   * @return Array[Char] representation of values in Pixel
   * */
  def getChar(): Array[Char]
}
