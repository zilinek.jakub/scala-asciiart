package App.models.Pixel

case class Char_Pixel(char : Char) extends Pixel {
  def get(): Char = char
  def getChar(): Array[Char] = Array(char)
}
