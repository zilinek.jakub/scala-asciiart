package App.models.Pixel

case class Gray_Pixel(value: Int) extends Pixel {
  def get(): Int = value

  override def getChar(): Array[Char] = {
    value.toString.toCharArray
  }

}
