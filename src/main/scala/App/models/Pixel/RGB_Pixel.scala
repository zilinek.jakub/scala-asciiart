package App.models.Pixel

import scala.collection.mutable.ArrayBuffer

case class RGB_Pixel(values : List[Int]) extends Pixel{
  def get(): List[Int] = values
  override def getChar(): Array[Char] = {
    var ret : ArrayBuffer[Char] = ArrayBuffer()
      for (i <- List(0,1,2)){
        for ( j <- values.apply(i).toString.toCharArray)
          ret += j
      }
    ret.toArray
  }
}
