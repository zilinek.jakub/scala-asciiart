package App.models.Image

import App.models.Grid.Grid
import App.models.Pixel.Pixel

/** Image representation
 * @tparam T is subtype of Pixel from which is image created
 *
 * */
trait Image[+T <: Pixel]{
  var _grid : Grid[Pixel]


  def grid : Grid[Pixel] = _grid
  def grid_ ( newgrid: Grid[Pixel]): Unit =  {_grid=newgrid}
}