package App.models.Image

import App.models.Grid.Grid
import App.models.Pixel.{Pixel, RGB_Pixel}

/** RGBImage Pixels are represented as chars
 *  @constructor takes Gird made out of RGB_Pixels
 * */
case class RGBImage(override val grid: Grid[RGB_Pixel]) extends Image[RGB_Pixel]{

  override var _grid: Grid[Pixel] = _
}
