package App.models.Image

import App.models.Grid.Grid
import App.models.Pixel.{Gray_Pixel, Pixel}

/** GrayImage Pixels are represented as chars
 *  @constructor takes Gird made out of Gray_Pixels
 * */
case class GrayImage(override val grid: Grid[Gray_Pixel]) extends Image[Gray_Pixel] {

  override var _grid: Grid[Pixel] = _
}
