package App.models.Image

import App.models.Grid.Grid
import App.models.Pixel.{Char_Pixel, Pixel}

/** CharImage Pixels are represented as chars
 *  @constructor takes Gird made out of Char_Pixels
 * */
case class CharImage(override val grid: Grid[Char_Pixel]) extends Image[Char_Pixel]{

  override var _grid: Grid[Pixel] = _
}
