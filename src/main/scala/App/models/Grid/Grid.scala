package App.models.Grid

import App.models.Pixel.Pixel

/**
 * Image represetation of 2d array
 * @tparam T is subtype of Pixel
 * @constructor needs 2d (Iterable(Iterable)) representation of Pixel
 * */
case class Grid[+T <: Pixel](pixels : Iterable[Iterable[T]]) {
}
