package App.exporters.Text

import App.exporters.Exporter

/**
 * Export text somewhere
 *
 */
trait TextExporter extends Exporter[String, Unit]{

}
