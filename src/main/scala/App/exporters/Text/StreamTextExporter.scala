package App.exporters.Text

import java.io.OutputStream

/**
 * Exports TextGrid to stream
 * @param outputStream where export
 */
class StreamTextExporter(outputStream: OutputStream) extends TextExporter
{
  private var closed = false

  /**
   * TextGrid to export to stream
   * @param text to export
   */
  protected def exportToStream(text: String): Unit ={

    if (closed)
      throw new Exception("The stream is already closed")

    outputStream.write(text.getBytes("UTF-8"))
    outputStream.flush()
  }

  def close(): Unit = {
    if (closed)
      return

    outputStream.close()
    closed = true
  }

  override def export(item: String): Unit = exportToStream(item)
}

