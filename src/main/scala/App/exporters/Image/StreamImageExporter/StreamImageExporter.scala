package App.exporters.Image.StreamImageExporter

import App.exporters.Image.ImageExporter
import App.models.Image.Image
import App.models.Pixel.Pixel

import java.io.OutputStream

/**
 * Exports Image to stream
 * @param outputStream where export
 */
case class StreamImageExporter(outputStream: OutputStream) extends ImageExporter[Unit] {
  private var closed = false
  /**
   * Image export to stream
   * @param img to export
   */
  protected def exportToStream(img: Image[Pixel]): Unit = {
    if (closed)
      throw new Exception("The stream is already closed")
    for (row <- img.grid.pixels) {
      for (col <- row)
        outputStream.write(col.getChar().map(_.toByte))
      outputStream.write('\n')
    }
  }
  def close(): Unit = {
    if (closed)
      return
    outputStream.close()
    closed = true
  }
  override def export(item: Image[Pixel]): Unit = exportToStream(item)
}

