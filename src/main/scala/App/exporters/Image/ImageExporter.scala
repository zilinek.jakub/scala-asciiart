package App.exporters.Image

import App.exporters.Exporter
import App.models.Image.Image
import App.models.Pixel.Pixel

/**
 * Export Image somewhere
 * */
trait ImageExporter[D] extends Exporter[Image[Pixel], D] {

}
