package App.exporters


/** @tparam D is the output of exporting
 *  @tparam T is type of Object that will be exported
 * */
trait Exporter[T, D] {

  /**
   * Exports something somewhere
   * @param item The item to export
   */
  def export(item: T): D

}
