package App.converters

import App.models.Grid.Grid
import App.models.Image.{CharImage, GrayImage}
import App.models.Pixel.Char_Pixel

import scala.collection.mutable.ListBuffer

object Gray2CharImage extends Convertor[GrayImage, CharImage]{

  /**
   * Convert GrayImage to CharImage
   *
   * @param item The item to convert
   */
  override def convert(item: GrayImage): CharImage = {
    val result : ListBuffer[ListBuffer[Char_Pixel]] = ListBuffer()
    for (row <- item.grid.pixels){
      var tmp : ListBuffer[Char_Pixel] = ListBuffer()
      for (pix <- row){
        tmp.addOne(Char_Pixel(Gray2ASCIIChar.convert(pix.get())))
      }
      result.addOne(tmp)
    }
    CharImage(Grid[Char_Pixel](result))
  }
}
