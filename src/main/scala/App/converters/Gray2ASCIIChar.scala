package App.converters

import scala.math.floor

object Gray2ASCIIChar extends Convertor[Int, Char]{
  var _ASCIIString : String = "@%#*+=-:. "

  /**
   * Convert Int to Char
   *
   * @param item The item to convert
   *
   * Takes item as grayscale representation of pixel and convert it acordinly to value to Char
   */
  override def convert(item: Int): Char = {
    if (_ASCIIString.isEmpty){
      return ' '
    }
    var rate = 255 / _ASCIIString.length
    var index = floor(item / rate).toInt
    if (index > 0)
      index-=1
    _ASCIIString(index)
  }
}
