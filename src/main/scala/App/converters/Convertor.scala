package App.converters

/**
 * @tparam IN item to convert
 * @tparam OUT converted item
 */
trait Convertor[IN, OUT] {
  /**
   * Convert something to something
   * @param item The item to convert
   */
  def convert(item: IN): OUT
}
