package App.converters

import App.models.Grid.Grid
import App.models.Image.{GrayImage, RGBImage}
import App.models.Pixel.Gray_Pixel

import scala.collection.mutable.ListBuffer

object RGB2GrayImage extends Convertor [RGBImage, GrayImage]{
  /**
   * Convert RGBImage to GrayImage
   *
   * @param item The item to convert
   */
  override def convert(item: RGBImage): GrayImage = {
    val res : ListBuffer[ListBuffer[Gray_Pixel]] = ListBuffer()

    for ( it <- item.grid.pixels ){
      val row : ListBuffer[Gray_Pixel] = ListBuffer()
      for ( it2 <- it ) {
        var toGray = (it2.get().head * 0.3 + it2.get().apply(1) * 0.59 + it2.get().apply(2) * 0.11)
        if ( toGray < 0 )
          toGray = 0
        else if (toGray > 255)
          toGray = 255
        row += Gray_Pixel(toGray.toInt)
      }
      res += row
    }
    GrayImage(Grid[Gray_Pixel](res))
  }
}
