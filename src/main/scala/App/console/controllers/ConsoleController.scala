package App.console.controllers
import App.console.views.pages.TextPage
import App.console.views.pages.concrete.HelpPage
import App.console.views.pages.generic.ErrorResponse
import App.converters.{Gray2CharImage, RGB2GrayImage}
import App.exporters.Image.StreamImageExporter.StreamImageExporter
import App.exporters.Text.TextExporter
import App.filters.charImage.GrayImageFilter
import App.importers.ImageImporter.{RGBImageFileImporter, RandomImageImporter}
import App.models.Image.RGBImage

class ConsoleController(stdOutputExporter: TextExporter) extends Controller {

  protected def render(renderer: TextPage): Unit ={
    val output = renderer.render()
    stdOutputExporter.export(output)
  }

  override def showHelp(): Unit = {
    render(new HelpPage)
  }

  override def showError(error: String): Unit = render(new ErrorResponse(error))

  /**
   * @param img        RGBImage that will be processed
   * @param correctors correctors that will be applied to image
   *                   Process RGB Photo specified with path parameter
   * @param exporters  List of exporters that we will export image to
   * */
  override def processRGBPhoto(img: RGBImage, correctors: GrayImageFilter, exporters: Iterable[StreamImageExporter]): Unit = {
    var grayImg = RGB2GrayImage.convert(img)
    grayImg = correctors.filter(grayImg)
    var ascii = Gray2CharImage.convert(grayImg)
    for (exp <- exporters){
      exp.`export`(ascii)
    }
  }

  override def importImage(path: String): Option[RGBImage] = {
    RGBImageFileImporter(path).importItem()
  }

  override def importRandomImage(): RGBImage = {
    RandomImageImporter(10,10).importItem().get
  }
}
