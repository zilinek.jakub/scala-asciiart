package App.console.controllers

import App.exporters.Image.StreamImageExporter.StreamImageExporter
import App.filters.charImage.GrayImageFilter
import App.filters.charImage.defaults.GrayImageIdentityFilter
import App.models.Image.RGBImage

trait Controller
{

  /**
   * Show help if user need to be informed
   */
  def showHelp(): Unit

  /**
   * render error with message passed in parameter
   * @param error is error message
   * */
  def showError(error: String): Unit

  /**
   * import image from path
   * @param path is path to image
   * @return Option of image, if found Option.some if not Option.empty
   * */
  def importImage(path: String) : Option[RGBImage]

  /**
   * generate random image
   * @return randomly generated image
   * */
  def importRandomImage() : RGBImage

  /**
   * @param img RGBImage that will be processed
   * @param correctors correctors that will be applied to image
   * Process RGB Photo specified with path parameter
   * */
  def processRGBPhoto(img: RGBImage, correctors: GrayImageFilter = GrayImageIdentityFilter, exporters: Iterable[StreamImageExporter]) : Unit
}
