package App.console.views

import App.console.controllers.Controller
import App.exporters.Image.StreamImageExporter.{FileOutExporter, StdOutExporter, StreamImageExporter}
import App.filters.charImage.GrayImageFilter
import App.filters.charImage.mixed.MixedFilter
import App.filters.charImage.specific.{BrightnessFilter, FlipFilter, InvertFilter}
import App.models.Image.RGBImage

import java.io.File
import scala.collection.mutable.ListBuffer

class ConsoleView(controller: Controller) {

  var image: Option[RGBImage] = Option.empty
  var filter : MixedFilter = _
  var output : ListBuffer[StreamImageExporter] = ListBuffer()


  /**
   * removes all indexes from list
   * @param ind list on indexes to remove
   * @param listBuffer array from which we remove
   * @return listBuffer with removed indexes
   * */
  protected def removeIndex(ind: List[Int], listBuffer: ListBuffer[String]) : ListBuffer[String] = {
    for (i <- ind.sortWith((s: Int, t: Int) => s > t))
      listBuffer.remove(i)
    listBuffer
  }

  /**
   * Finds --image or --image-random and uses them to import image
   * if --image was specified wrongly we notify user and set this.image to Option.empty -> will be checked later
   * @return commands without processed parameters
   * */
  protected def loadInputSource(commands: ListBuffer[String]): ListBuffer[String] = {
    for (i <- commands.indices){
      if (commands.apply(i) == "--image") {
        if (i == commands.size - 1) {
          return commands
        }
        image = controller.importImage(commands.apply(i + 1))
        return removeIndex(List(i,i+1), commands)
      }
      if (commands.apply(i) == "--image-random") {
        image = Option(controller.importRandomImage())
        return removeIndex(List(i), commands)
      }
    }
    commands
  }
  /**
   * Finds --output*
   * if --output* was specified wrongly we notify user and ignores the poarameter
   * @return commands without processed parameters
   * */
  protected def loadOutputEndPoints(commands: ListBuffer[String]): ListBuffer[String] = {
    val toRemove : ListBuffer[Int] = ListBuffer()

    for (i <- commands.indices){
      if (commands.apply(i) == "--output-console") {
        this.output.addOne(new StdOutExporter())
        toRemove.addOne(i)
      }
      if (commands.apply(i) == "--output-file") {
        try {
          this.output.addOne(new FileOutExporter(new File(commands.apply(i + 1))))
          toRemove.addAll(List(i,i+1))
        } catch {
          case _: Throwable =>
            return removeIndex(toRemove.toList, commands)
        }
      }
    }

    removeIndex(toRemove.toList, commands)
  }
  /**
   * Finds any filters and their parameters etc. --brightness --flip --invert
   * if parameter is specified wrongly we ignore it and stop parsing
   * @return commands without processed parameters
   * */
  protected def loadFilters(commands: ListBuffer[String]): ListBuffer[String] ={
    val filterList : ListBuffer[GrayImageFilter] = ListBuffer()
    val toRemove : ListBuffer[Int] = ListBuffer()
    for (i <- commands.indices){
      if (commands.apply(i) == "--invert") {
        filterList.addOne(new InvertFilter())
        toRemove.addOne(i)
      }
      else if(commands.apply(i) == "--brightness"){
        if (i == commands.size - 1 || commands.apply(i+1).toIntOption.isEmpty){
          return removeIndex(toRemove.toList, commands )
        }
        filterList.addOne(new BrightnessFilter(commands.apply(i+1).toInt))
        toRemove.addAll(List(i,i+1))
      }
      else if(commands.apply(i) == "--flip"){
        if (i == commands.size - 1 || (commands.apply(i+1) != "x" && commands.apply(i+1) != "y")){
          return removeIndex(toRemove.toList, commands )
        }
        else {
          filterList.addOne(new FlipFilter(commands.apply(i+1)))
          toRemove.addAll(List(i,i+1))
        }
      }
      else{
        // doesnt match any filter
      }
    }
    this.filter = new MixedFilter(filterList.toList)
    removeIndex(toRemove.toList, commands )
  }

  /**
   * Checks if all arguments were used if not notify user and prints help page
   * Check if output is specified if not, notify user
   * */
  protected def chechFoundArgs(commands: ListBuffer[String]): Boolean = {
    var outputflag = false
    var commandsflag = false

    if (output.isEmpty)
      outputflag = true

    if (commands.nonEmpty)
      commandsflag = true

    if (commandsflag){
      controller.showError("Some of arguments are not used, or not use but not properly:")
      for (i <- commands)
        controller.showError("\t" + i)
      controller.showError("\n\n")
    }
    if (outputflag){
      controller.showError("Output endpoint is not specified...")
    }
    if (commandsflag || outputflag)
      return false
    true
    }

  /**
   * Check if image is specified, if not notify user
   * */
  protected def checkInput() : Boolean = {
    if(image.isDefined){
      return true
    }
    controller.showError("Invalid image type or path to image\n\n")
    false
  }


  /**
   * main run function of view, parses all commands using methods above
   * */
  def processCommand(commands: Seq[String]): Unit ={
    if (commands.contains("help")) {
      controller.showHelp()
      return
    }

    val cmds : ListBuffer[String] = ListBuffer()

    for (i <- commands)
      cmds.addOne(i)


    loadInputSource(cmds)

    // need to check if we have valid input before we generate output -> if we wouldnt we would create empty output file
    if (checkInput()) {
      loadFilters(cmds)

      loadOutputEndPoints(cmds)

      if (!chechFoundArgs(cmds)) {
        controller.showHelp()
      }
      else
        controller.processRGBPhoto(image.get, filter, output)
    }
    else
      controller.showHelp()
  }
}