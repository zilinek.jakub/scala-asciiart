package App.console.views.pages.generic

import App.console.views.pages.TextPage

class ErrorResponse(message: String = "Error") extends TextPage
{
  override def render(): String = message + "\n"
}
