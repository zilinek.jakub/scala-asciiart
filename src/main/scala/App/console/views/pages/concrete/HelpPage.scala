package App.console.views.pages.concrete

import App.console.views.pages.TextPage

class HelpPage extends TextPage
{
  override def render(): String = {
    var result = ""

    result += "-----COMMANDS-----\n"
    result += "Commands must be in order like: \n"
    result += "1. Input source\n"
    result += "\t--image $filepath - for specifying image to edit with path\n"
    result += "\t--image-random - for specifying image to be randomly generated\n\n"
    result += "2. List of image Correctors\n"
    result += "\t--flip x - to flip image with X axis\n"
    result += "\t--flip y - to flip image with X axis\n"
    result += "\t--brightness 60 - to lower the brightness to 60%\n\n"
    result += "3. Output\n"
    result += "\t--output-console - to output result to console\n"
    result += "\t--output-file $path - to output result to file at $path\n"


    result
  }
}
